## Problems

1. Create the problem's folder under "code" folder.
2. Create README file with problem specs.
3. Create Test file with sample inputs and results.
4. Create Module/Pod file(s) with solution/documentation.
5. Create Lesson file with notes, concepts, links, hints, best practices, etc.
6. Create Polices (.perlcriticrc) file with related rules.
7. Create cpanfile with dependencies


