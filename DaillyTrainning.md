# Daily Code Trainning

1. Typing skills 
    1. (keybr.com)[http://keybr.com]
2. Problem Solving skills
    1. the problem's folder.
    2. Readme file with problem specs and some technical .
    3. Test file with sample inputs and results.
    4. Code file(s) with solution/documentation.
    5. Lesson file with notes, concepts, links, hints, best practices, etc.
    6. Polices file with related rules.
    7. Manifest with current files, coupling, cohesion and dependencies
3. Language skills 
    1. Variables
    2. Control Structures
    3. Data Structures
    4. Syntax
    5. Tools
        * Tests
        * Refactor
        * Debug
        * Lint
        * Perf
        * Count
4. Computer Science Skills
    1. (MIT Courses)[https://ocw.mit.edu/courses/electrical-engineering-and-computer-science]
    2. (Wikipedia)[https://en.wikipedia.org/wiki/Software_engineering]
    2. (Wikipedia)[https://en.wikipedia.org/wiki/Software_development]
    2. (Wikipedia)[https://en.wikipedia.org/wiki/Computer_programming] 
