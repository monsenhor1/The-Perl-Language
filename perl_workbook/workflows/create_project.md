# Create Project

## Project
- name, description, design, links
- members, stakeholders, contacts

## Infrastructure
- repos, instances
- backlog, boards
- services

## Company 
- billing, payment
- Mission, visions, polices, marketing
- domains

# Steps (Project Dashboard)

## Opening
0. Register Company
1. Register Project
2. Register Members

## Plan
0. Project description
1. Tasks

# Required
- Create Company
