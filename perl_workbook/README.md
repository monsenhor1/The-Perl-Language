# Learn by tasks

It's about the work of a devOps using perl as a main language and vim
as the main IDE.

The workflows can use a diversity of environments and languages, like
javascript, html, css, react, bootstrap, bash, sed, awk, C, C++, etc

># Doing now:

# Task management

We use taskwarrior.

- tasks created 
- Any task must begin with task start

------

># Backlog: To be done:

# Tasks

## Microservices

- Create a microservice that says "Hello There!". 
- Create a microservice showing the url domains of an internet project

## Pipes

- Pipe inserting a header and a footer, before and after the stream.
- Create a pipe that evaluates all code inside tags "<~( )~/>" 

## Sysadmin 

### CI/CD
- Sync diretories from projects, for redundancy and backup

### Infrastructure
- Setup services
- Setup domain

## Operation
- Manage a marketing campaign in Social Networks/E-mail/Groups
    * Manage Target - graded list of leads
		* Manage Goals and Plan - Backlogs, Tasks and Boards
		* Automate sending content
		* Automate reading and doing simple answers
		* Analyse responses and preview behaviors

