# Perl

by Monsenhor

## Synopsis

Run the presentation:

```
$ ./bin/the_perl_book.pl run
```

Run the webapp:

```
$ ./bin/the_perl_book.pl webapp
```

Compile the book in the directory ./build

```
$ ./bin/the_perl_book.pl compile
```

Solve exercises

```
$ ./bin/the_perl_book.pl exercises
```

Apply and obtain a certification

```
$ ./bin/the_perl_book.pl certification
```



## Description

It's a book about the Perl language in 4 volumes.

1. *The Perl Language*
2. *The Perl Engineer*
3. *Perl Workbook*
4. *Problems Solving with Perl*

[//]: # (
[![asciicast](https://asciinema.org/a/241951.svg)](https://asciinema.org/a/241951)
)

-> # Sign and be a Patron <-

Go now to [Patreon](https://www.patreon.com/monsenhor) and sign our tier.

Choose what you want in the book and help us making a great work. 
As a plus, actually you will be one of the most well-informed hackers.

-> _*ENJOY!!!*_ <-


## Organization

First read the documents:

- Toolkit.md
- DaillyTrainning.md
- ChaptersStructure.md
- LessonStructure.md
- ProblemStructure.md

### Slides and Lessons

1. Run the mojolicious app:

```
cd web
./run.pl
```

2. Use mdp to present slides:

```
sudo apt install mdp
mdp ./slides/README.md
mdp ./slides/Lesson_001.md
```

3. Use ascinema to play the terminal presentations:

```
asciinema play media/241951.cast
```


