#!/usr/bin/env perl

'''
Problem solving course

Problem 1. Add up to

Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

Bonus: Can you do this in one pass?

'''

# Functions

# is_up returns True if 2 numbers sum up to k
def is_up (the_list, up_to):
 for i in the_list:
  for j in the_list:
    if i + j == up_to:
      return True 
 return False 

#################################

# Read the list, numbers separated by spaces
numbers = raw_input("Enter a list of numbers: ")
k   = int ( raw_input("Enter the up to value: ") )
my_list = list( map(int, numbers.split()) )
print "Received input numbers is: ", my_list
print "Up to value is: ", k

# Test if the numbers sum up_to
if is_up(my_list, k):
  print "It's up to!"
else :
  print "It's not up to!" 


