#!/usr/bin/env perl
#ABSTRACT create_pdf_book.pl compiles the pdf book
use strict;
use warnings;
use feature "say";
use lib "lib";

use ThePerlBook;
ThePerlBook->run;

our $VERSION = 0.01;


=head1 NAME

create_pdf_book.pl

=head1 VERSION

Version 0.01

=head2 SYNOPSIS

        create_pdf_book.pl <command> [options]


=head1 DESCRIPTION

=head1 DEPENDENCIES

=head1 INCOMPATIBILITIES



=head1 BUGS AND LIMITATIONS

Please report any bugs or feature requests through the web interface at
L<https://github.com/rfilipo/create_pdf_book.pl/issues/new>.
I will be notified, and then you'll automatically be notified of progress on
your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

        perldoc create_pdf_book.pl


You can also look for information at:

=over

=item * GitHub's request tracker

L<https://github.com/rfilipo/create_pdf_book.pl/issues>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/create_pdf_book.pl>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/create_pdf_book.pl>

=item * MetaCPAN
L<https://metacpan.org/release/create_pdf_book.pl>

=back


=head1 AUTHOR

L<|https://metacpan.org/author/MONSENHOR>,
C<< <monsenhor at cpan.org> >>.


=head1 LICENSE AND COPYRIGHT

Copyright 2018 Ricardo Filipo.

This code is free software; you can redistribute it and/or modify it under the
same terms as Perl 5 itself.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the LICENSE file for more details.

=cut

__DATA__

__END__
