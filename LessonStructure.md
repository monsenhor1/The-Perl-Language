# Lesson Structure

## Lessons has at least the following items:
1. Skills
2. Topics
3. Material
    * Media
    * Code
4. Text
5. Exercise

## Workflow
1. Create a directory with the lesson file in markdown format.
2. List the items covered by the lesson.
3. Write the textual content of the lesson.
4. Prepare the exercises and problems for the lesson.
  - Each problem must be written separately in the _Problems Solving_ book.
5. Prepare the material, media, and code for the lesson.
6. Link the lesson to the main book.

