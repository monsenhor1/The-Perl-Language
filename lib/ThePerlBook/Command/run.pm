package ThePerlBook::Command::run;
use ThePerlBook -command;
use strict; use warnings;

use YAML::Tiny;
 
# Open the config
my $book = YAML::Tiny->read( 'book.yml' );

sub abstract { "Run slide series" }
 
sub description { "Run slide classes. If used option \"-s\", it runs a specific slide" }
 
sub opt_spec {
  return (
    [ "slide|s"  ,  "plays the very slide"       ],
    [ "verbose|v",  "show all, verbosely"       ],
  );
}
 
sub validate_args {
  my ($self, $opt, $args) = @_;
 
  # no args allowed but options!
  $self->usage_error("No args allowed") if @$args;
}
 
sub execute {
  my ($self, $opt, $args) = @_;
  
  my $verbose = ""; $verbose = "--verbose" if $opt->{verbose};
 
  my $result = $opt->{pdf} ? run_pdf($verbose) : run($verbose);
 
  print $result;
}

=head1 run

ThePerlBook is made of markdown, asciidoc, pod, asciinema, xm (exercism files), html, etc

When compiling the book all files referenced as B<toc> in the file I<book.ini> will be processed in a DocBook entity and saved in the I<build> directory. 


=cut

sub run {
  my $args = shift;
  my $files = "";
 
  my $slides = $book->[0]->{slides};
  foreach my $file (@{$slides}){
     print "Opening $file ...\n\n";
     my $key = getc(STDIN);
     system "mdp $file";
  }
  return "Thank you for using The Perl Book!!!\n" 
}

1;
