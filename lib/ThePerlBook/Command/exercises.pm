package ThePerlBook::Command::compile;
use ThePerlBook -command;
use strict; use warnings;

use YAML::Tiny;
 
# Open the config
my $book = YAML::Tiny->read( 'book.yml' );

sub abstract { "Compile the book" }
 
sub description { "Book is created in the directory \"build\". If used option \"-p\", a pdf file will be generated as well" }
 
sub opt_spec {
  return (
    [ "pdf|p",  "generate a pdf" ],
    [ "verbose|v",  "show all, verbosely"       ],
  );
}
 
sub validate_args {
  my ($self, $opt, $args) = @_;
 
  # no args allowed but options!
  $self->usage_error("No args allowed") if @$args;
}
 
sub execute {
  my ($self, $opt, $args) = @_;
  
  my $verbose = ""; $verbose = "--verbose" if $opt->{verbose};
 
  my $result = $opt->{pdf} ? compile_pdf($verbose) : compile($verbose);
 
  print $result;
}

=head1 compile

ThePerlBook is made of markdown, asciidoc, pod, asciinema, xm (exercism files), html, etc

When compiling the book all files referenced as B<toc> in the file I<book.ini> will be processed in a DocBook entity and saved in the I<build> directory. 


=cut

sub compile {
  my $args = shift;
  my $files = "";
 
  my $toc = $book->[0]->{toc};
  foreach my $file (@{$toc}){
     $files .= " ".$file;
  }
  my $command = "pandoc $args -o build/theperllanguage.xml -t docbook $files";
  print $command."\n";
  system $command;
  return "Book Compiled!!!" 
}

sub compile_pdf {
  my $args = shift;
  my $files = "";
  print compile($args), "\n";
  my $toc = $book->[0]->{toc};
  foreach my $file (@{$toc}){
     $files .= " ".$file;
  }  my $command = "pandoc $args -o build/theperllanguage.pdf $files";
  print $command."\n";
  system $command;
  return "PDF Book Compiled!!!" 
}

1;
